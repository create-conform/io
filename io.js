/////////////////////////////////////////////////////////////////////////////////////////////
//
// io
//
//    Library for reading and writing data.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var event =  require("event");
var access = require("io-access");
var stream = require("io-stream");
var uri =    require("io-uri");
var volume = require("io-volume");
             require("buffer-subarray");

var io;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// IO Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function IO() {
    var self = this;

    var volumes = {};
    this.volumes = {};
    this.volumes.get = function (opt_id) {
        var found = [];
        for (var v in volumes) {
            if (opt_id && volumes[v].id == opt_id) {
                found.push(volumes[v]);
            }
        }
        return found;
    };
    this.volumes.register = function (volume) {
        var exists = false;
        for (var v in volumes) {
            if (volumes[v] == volume) {
                exists = true;
                break;
            }
        }
        if (exists) {
            throw new Error(self.ERROR_VOLUME_ALREADY_REGISTERED, "Volume '" + volume.name + "' is already registered.", volume);
        }

        volumes.push(volume);

        self.Volume.events.fire(self.EVENT_VOLUME_ADDED, volume);
    };
    this.volumes.unRegister = function (volume) {
        for (var v in volumes) {
            if (volumes[v] == volume) {
                volumes.splice(v, 1);
                self.Volume.events.fire(self.EVENT_VOLUME_REMOVED, volume);
                return;
            }
        }

        throw new Error(self.ERROR_VOLUME_NOT_FOUND, "Volume '" + volume.name + "' was not registered.", volume);
    };
    this.volumes.events = new event.Emitter(this);
}
IO.prototype.ERROR_FILE_SIZE_EXEEDS_LIMIT =    "File Size Exeeds Limit";
IO.prototype.EVENT_VOLUME_ADDED =              "volume-added";
IO.prototype.EVENT_VOLUME_REMOVED =            "volume-removed";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Singleton Instance
//
/////////////////////////////////////////////////////////////////////////////////////////////
io = new IO();

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = io;